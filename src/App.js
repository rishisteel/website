import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home/";
import Contact from './pages/contact/'
import Navbar from "./components/Navbar/";
import CreateAtt from "./components/Attandance/CreateAtt";
import Product from './pages/product/'
import './App.css';
import QualityPolicy from "./pages/policy/QualityPolicy";
import About from "./pages/about/";
import Footer from './components/footer'

function App() {
  return (
    <div>
      <Navbar />
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/contact-us"  component={Contact} />
          <Route path="/att"  component={CreateAtt} />
          <Route path="/product"  component={Product} />
          <Route path="/quality-policy" component={QualityPolicy} />
          <Route path="/about" component={About} />
        </Switch>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
