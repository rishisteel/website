import React, { useEffect, useState } from "react";

const CreateAtt = () => {
  const [LIVE_DATE_TIME, setLIVE_DATE_TIME] = useState();
  useEffect(() => {
    setInterval(() => {
      const CurrentDateTime = new Date().toLocaleString();
      setLIVE_DATE_TIME(CurrentDateTime);
    }, 1000);
  }, []);

  return (
    <div className="p-5 container">
      <p className="text-center">
        <b> {LIVE_DATE_TIME} </b>
      </p>
      <div className="form-group">
        <label>Enroll No:</label>
        <input type="text" className="form-control" />
      </div>
      <p>Name : Manjunatha V M</p>
      <p>Plant : Hirehalli</p>
      <p>Role: Developer</p>
      <div className="form-group">
        <label>Status</label>
        <select className="form-control">
          <option></option>
          <option>IN</option>
          <option>OUT</option>
        </select>
      </div>
      <div className="form-group">
        <label>Remarks:</label>
        <textarea className="form-control"></textarea>
      </div>
      <div className="form-group">
        <button className="btn btn-primary">Submit</button>
      </div>
    </div>
  );
};

export default CreateAtt;
