import React, {useState, useEffect} from "react";
import Logo from "../../assets/logo.JPG";

const NavDesktop = () => {

    const [path, setPath] = useState(null)
    useEffect(() => {
       setPath(window.location.pathname)
    }, [])

  return (
    <div id="navbar">
      <a href="/">
        <img className="logo-desktop" src={Logo} alt="rishi steel tubes logo" />
      </a>
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li >
        <a href="/about" className={path === "/about" && "active"}>About Us</a>
        </li>
        <li>
          <a href="/c">Products</a>
        </li>
        <li>
          <a href="/quality-policy" className={path === "/quality-policy" && "active"}>Quality Policy</a>
        </li>
        <li>
          <a href="/a">Enquiry</a>
        </li>
        <li >
          <a className={path === "/contact-us" && "active"}href="/contact-us">Contact</a>
        </li>
      </ul>
    </div>
  );
};

export default NavDesktop;
