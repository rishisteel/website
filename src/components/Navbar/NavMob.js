import React, {useState} from "react";
import Logo from "../../assets/logo.JPG";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from '@material-ui/core/Drawer';
import { BorderBottom } from "@material-ui/icons";

const NavMob = () => {
    const [openDraw, setOpenDraw] = useState(false)
  return (
    <div style={{ padding: "5px" }} id="navbarMobile">
         <Drawer width={500} anchor={'right'} openSecondary={true} open={openDraw} onClose={() => setOpenDraw(false)}>
             <div style={{paddingLeft:"15px", paddingRight:"15px", width:"200px"}}>
                 <p><a href="/">Home</a></p>
                 <p>About</p>

             </div>
        </Drawer>

      <a href="/">
        <img className="logo-mobile" src={Logo} alt="rishi steel tubes logo" />
      </a>
      <div style={{ float: "right" }}>
        <IconButton edge="start" color="inherit" aria-label="menu" onClick={() => setOpenDraw(true)}>
          <MenuIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default NavMob;
