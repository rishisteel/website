import React from 'react'
import NavDesktop from './NavDesktop'
import NavMobile from './NavMob'
import './nav.css'

const index = () => {
    const windowSize = window.innerWidth
    if(windowSize > 1024){
        return (
            <div>
            <NavDesktop />
        </div>
        )
    } else
    return (
        <div>
            <NavMobile />
        </div>
    )
}

export default index
