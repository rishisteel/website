import { Container } from "@material-ui/core";
import { Box } from "@material-ui/core";
import React from "react";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';

const index = () => {
  return (
    <Box p={10}>
      <Container>
        <Grid container>
          <Grid item lg={6}>
            Copyright © 2021 Rishi Steels & Tubes. All rights reserved
          </Grid>
          <Grid item xs={6} >
              <div style={{float:'right'}}>
            <FacebookIcon  />
            <InstagramIcon style={{marginLeft:'15px'}} />
            <LinkedInIcon style={{marginLeft:'15px'}} />
            </div>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

const useStyle = makeStyles({
    socialIcon: {
        marginLeft: '13px'
    }
})

export default index;
