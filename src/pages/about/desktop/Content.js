import { Box, Container } from "@material-ui/core";
import React from "react";

const Content = () => {
  return (
    <Box>
      <Container className="text-center about-us">
        <h3>About Us</h3>
        <p>
          Established in the year 2015, We Rishi Steels & Tubes are one of the
          leading manufacturer, specialized in producing stainless steel pipes
          in Bangalore. In 2015, in order to meet great demand of the Bangalore
          market we started our factory to initially supply various kind of
          stainless steel pipes/tubes, In different shapes like Round, Square,
          Rectangular Sections in SS 304, SS 316, and JT Grades. With a team of
          professionals, we have successfully obtained good response from our
          domestic market.
        </p>
        <p>
          We are continuously making the best efforts to improve the quality of
          our products and business effectiveness in order to bring you the best
          Quality, Products & services so that any of your demands should be
          promptly satisfied.
        </p>
      </Container>
    </Box>
  );
};

export default Content;
