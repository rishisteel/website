import React from 'react'
import AboutDesktop from './desktop/'

const index = () => {
    return (
        <div>
            <AboutDesktop />
        </div>
    )
}

export default index
