import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Box, Container, Grid } from "@material-ui/core";
import EmailIcon from "@material-ui/icons/Email";
import CallIcon from "@material-ui/icons/Call";
import RoomIcon from "@material-ui/icons/Room";
import ContactForm from "./ContactForm";

const useStyles = makeStyles({
  root: {
    minWidth: 295,
    margin: 5,
    minHeight:100
  },
  title: {
    fontSize: 14,
  },
  titleText: {
      paddingLeft:5
  },
  pos: {
    marginBottom: 12,
  },
  gridSpace: {
      display: 'flex',
  }
});

function ContactCard() {
  const classes = useStyles();

  return (
    <Box>
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15552.212027289946!2d77.500539!3d12.96846!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x23b748009ba890a5!2sRishi%20Steels%20%26%20Tubes!5e0!3m2!1sen!2sin!4v1625045046733!5m2!1sen!2sin" width="100%" height="200" frameborder="0" style={{"border":0}} allowfullscreen></iframe>
      <Container>
      <p>Call Us or Fill the Form</p>
          <Grid container className="pt-5">
          <Grid sm={1}>
              </Grid>
              <Grid sm={4}>
<ContactForm />
              </Grid>
              <Grid sm={6} className="pt-5">
              <Grid container>
          
          <Grid sm={6} className={classes.gridSpace}>
            <Card className={classes.root}>
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                }}
                >
                  <CallIcon />
                 <span className={classes.titleText}> Phone </span>
                </Typography>

                <Typography variant="body2" component="p">
                  98866 66630
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid sm={6} className={classes.gridSpace}>
            <Card className={classes.root}>
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                }}
                >
                  <EmailIcon />
                  <span className={classes.titleText}> Email </span>
                </Typography>
                <Typography variant="body2" component="p">
                  info@rishisteeltubes.com <br />
                  nayak.prakhyat@rishisteeltubes.com
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid sm={12} className={classes.gridSpace}>
            <Card className={classes.root}>
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                }}
                >
                  <RoomIcon />
                  <span className={classes.titleText}> Address</span>
                </Typography>

                <Typography variant="body2" component="p">
                  2nd Floor, Yashaswini Layout, ITI Employees Layout, Main Road
                  Mallathahalli, Nagarabhavi 2nd Stage, Bengaluru, Karnataka
                  560072
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          
        </Grid>
              </Grid>
          </Grid>
       
      </Container>
    </Box>
  );
}

export default ContactCard;
