import { Box, Button, Container, Grid, TextField } from "@material-ui/core";
import React from "react";

const ContactForm = () => {
  return (
    <Box>
      <Container>
        <Grid container>
          <Grid item sm={12} className="pt-5">
            <form>
              <TextField
                label="Name"
                size="small"
                 variant="outlined"
                 fullWidth
              />{" "}
              <br />
              <br />
              <TextField label="Mobile Number" size="small" variant="outlined" fullWidth />
              <br />
              <br />
              <TextField label="Email" size="small" variant="outlined" fullWidth />
              <br />
              <br />
              <TextField label="Message" rows={4} multiline size="small" variant="outlined" fullWidth />
              <Grid item className="pt-3">
              <Button variant="contained" color="secondary">
                  Send Us
              </Button>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default ContactForm;
