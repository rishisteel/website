import React from 'react'
import ContactCard from './ContactCard'
import ContactForm from './ContactForm'

const index = () => {
    return (
        <div>
            <ContactCard />
        </div>
    )
}

export default index
