// import Swiper core and required modules
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { Box } from '@material-ui/core';

// install Swiper modules
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

export default () => {
  return (
    <Box>
        <h1>Industries We Serve</h1>
    <Swiper
      spaceBetween={0}
      slidesPerView={5}
      navigation
      scrollbar={{ draggable: false }}
      onSwiper={(swiper) => console.log(swiper)}
      onSlideChange={() => console.log('slide change')}
    >
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
      <SwiperSlide><img src="https://3.imimg.com/data3/IT/JJ/MY-9119201/mild-steel-railings-500x500.jpg" width='100%' alt="rishi" /></SwiperSlide>
    </Swiper>
    </Box>
  );
};