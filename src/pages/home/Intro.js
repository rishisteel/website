import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Container, Grid, Link } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    maxWidth: 800,
  },
  media: {
    height: 240,
  },
});
const ProductCard = () => {
  const classes = useStyles();
  return (
    <div className="pt-5">
      <Container>
        <Grid container>
          <Grid item sm={6} className="p-5">
            <Card className={classes.root}>
            <Link href="/product" style={{textDecoration: 'none'}}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="/static/media/slider1.ae60b88a.jpg"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Stainless steel Round pipes / Tubing –ASTM standards
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Apollo Coastguard products are made of special hot dipped
                    galvanized steel coil to cater to the need of galvanized
                    steel tubes in the coastal regions. These pre-galvanized
                    tubes have a rich interior.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Link>

              <CardActions>
                <Button size="small" color="primary">
                  Read More...
                </Button>
                <Button size="small" color="primary">
                  Ask Quotation
                </Button>
              </CardActions>
            </Card>

          </Grid>

          <Grid item sm={6} className="p-5">
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://i0.wp.com/www.apex-steel.com/wp-content/uploads/2018/05/cold-formed-rectangular-hollow-sections.jpg?resize=900%2C600&ssl=1"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                  Stainless steel welded square / Rectangular Tubing –ASTM standards
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Apollo Coastguard products are made of special hot dipped
                    galvanized steel coil to cater to the need of galvanized
                    steel tubes in the coastal regions. These pre-galvanized
                    tubes have a rich interior.
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Read More...
                </Button>
                <Button size="small" color="primary">
                  Ask Quotation
                </Button>
              </CardActions>
            </Card>
          </Grid>

          
        </Grid>
      </Container>
    </div>
  );
};

export default ProductCard;
