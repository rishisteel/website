import React from "react";
import { Grid, Box, Container, Typography, Button } from "@material-ui/core";
import Slider1 from "../../assets/slider1.jpg";
import Store from '../../assets/store.jpg'

const Introduction = () => {
  return (
    <Box>
      <div style={{position:'relative'}}>
      <img src={Slider1} width="100%" height="auto" alt="rishi steel tubes" style={{filter: 'brightness(30%)'}} />
      <div className='header'>
      <h1>
        MANUFACTURING STAINLESS STEEL TUBES SINCE 2015.
      </h1>
      <center>
      <button className='button-background'>Our Products</button>
      <button className='button-background2'>Get Quote</button>
      </center>

      </div>
      </div>
      <Container>
        <Grid container style={{paddingTop:'40px'}}>
          
          <Grid item lg={6}>
            <div className='home-about'>
          <h2>Welcome to Rishi Steels & Tubes</h2>
            <p>
              Established in the year 2015, We Rishi Steels & Tubes are one of
              the leading manufacturer, specialized in producing stainless steel
              pipes in Bangalore. In 2015, in order to meet great demand of the
              Bangalore market we started our factory to initially supply
              various kind of stainless steel pipes/tubes, In different shapes
              like Round, Square, Rectangular Sections in SS 304, SS 316, and JT
              Grades
            </p>
            <div className="pt-1">
            <Button variant="contained" color="secondary">Know More</Button>
            </div>
            </div>
          </Grid>
          
          <Grid item lg={6}>
            <img src={Store} alt='rishi steel tubes' className="img-center p-5" style={{borderRadius:'30px'}} width="80%" />
          </Grid>
        </Grid>
     
      </Container>
    </Box>
  );
};

export default Introduction;
