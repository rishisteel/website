import { Box, Container, Grid } from "@material-ui/core";
import React from "react";

const OurValue = () => {
  return (
    <Box >
          <h1>Why Rishi</h1>

      <Container className='text-center'>
        <Grid container>
          <Grid item lg={3}>
            <p className='reach-header'>Quality</p>
            <p>Tons Manufactuered</p>
          </Grid>

          <Grid item lg={3}>
            <p className='reach-header'>200+</p>
            <p>Satisfied Dealers</p>
          </Grid>

          <Grid item lg={3}>
            <p className='reach-header'>100+</p>
            <p>Satisfied Employees</p>
          </Grid>

          <Grid item lg={3}>
            <p className='reach-header'>30</p>
            <p>Serve Cities in the South India</p>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default OurValue;
