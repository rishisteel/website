import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const Slider = () => {
    return (
        <div style={{position:'relative'}}>
             <Carousel showThumbs={false} autoPlay={true} showStatus={false}>
                <div>
                    <img src="https://www.tamilnadusteeltubesltd.com/images/slide-show2.jpg" />
                </div>
                <div>
                <img src="https://www.tamilnadusteeltubesltd.com/images/slide-show2.jpg" />
                </div>
                <div>
                <img src="https://www.tamilnadusteeltubesltd.com/images/slide-show2.jpg" />
                </div>
            </Carousel>
        </div>
    )
}

export default Slider
