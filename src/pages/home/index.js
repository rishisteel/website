import React from 'react'
import Certification from './Certification'
import Industries from './Industries'
import Introduction from './Introduction'
import OurValues from './OurValues'
import ProductCard from './ProductCard'
import Reached from './Reached'
import './style.css'

const index = () => {
    return (
        <div>
            <Introduction />
            <Reached />
            <ProductCard />
            <OurValues />
            <Industries />
            <Certification />
        </div>
    )
}

export default index
