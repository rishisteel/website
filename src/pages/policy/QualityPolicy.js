import { Box, Container, Grid, Typography } from "@material-ui/core";
import React from "react";
import './style.css'

const QualityPolicy = () => {
  return (
    <Box>
      <div
        style={{
          backgroundColor: "#01a501",
          color: "white",
          padding: "50px 50px 50px 50px",
        }}
      >
        <Container item>
          <h1>Quality Policy</h1>
        </Container>
      </div>
      <Container className="pt-5 ">
        <Container item >
          <Grid sm={12}>
            <p className='quality-section'>
              Followed with the conception based on honesty, win with quality,
              perfect service, customer first, quality assurance has been an
              integral part of the company's history. Quality Assurance begins
              at the raw material stage itself. Material is inspected for
              chemical composition, and tested for other parameters like
              mechanical properties, gauge variation, etc. At Rishi Steels and
              Tubes, each product is processed according to the norms sequenced
              by the process control personnel based on stringent international
              standards and monitored through uncompromising quality control
              tests at every stage.
            </p>
            <br />

            <p className='quality-section'>
              To ensure quality, every tube is checked for dimension by
              qualified personnel. We strictly adhere to Indian and
              International standards. The manufactured ERW tubes are made to
              undergo the following tests :
            </p>
            <br />
            <div className='quality-section'>
            <li>Tensile testing machine</li>
            <li>Hardness tester</li>
            <li>Bend test</li>
            <li>Flattening test</li>
            <li>Straightness test</li>
            <li>Drift test</li>
            <li>Cupping test</li>
            </div>
          </Grid>
        </Container>
      </Container>
    </Box>
  );
};

export default QualityPolicy;
