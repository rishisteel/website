import React from "react";
import { Typography, Button } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

const index = () => {
  return (
    <Box m={5}>
      <Container maxWidth="lg">
        <Grid container>
          <Grid item sm={6}>
            <img
              src="https://aplapollo.com/wp-content/uploads/2020/08/Costgaurd-460.jpg"
              style={{
                display: "block",
                marginLeft: "auto",
                marginRight: "auto",
                width: "100%",
              }}
              alt="rishi stainless tubes"
            />
          </Grid>
          <Grid item sm={6}>
            <Typography variant="h4" >
            Stainless steel Round pipes / Tubing –ASTM standards
            </Typography>
            <Button variant="contained" color="primary" style={{marginTop:"15px"}}>Place Order</Button>
           <Typography style={{paddingTop:"15px"}}>
            Coastguard products are made of special hot dipped galvanized steel coil to cater to the need of galvanized steel tubes in the coastal regions. These pre-galvanized tubes have a rich interior as well as exterior coating of Zinc. The Coastguard is primarily being used for the purpose of roofing structures providing protection from rains and sunlight that can last for generations. They are available in Square, Rectangular, and Circular sections.
           </Typography>
          </Grid>

          <Grid item xs={12} m={3}>
            <Typography variant="h6" color="textSecondary">
              DESCRIPTION:
            </Typography>
            <Typography variant="body2" color="textSecondary">
               Coastguard products are made of special hot dipped
              galvanized steel coil to cater to the need of galvanized steel
              tubes in the coastal regions. These pre-galvanized tubes have a
              rich interior as well as exterior coating of Zinc. The Coastguard
              is primarily being used for the purpose of roofing structures
              providing protection from rains and sunlight that can last for
              generations. They are available in Square, Rectangular, and
              Circular sections.
            </Typography>
          </Grid>

          <Grid item xs={12} m={3}>
            <Typography variant="h6" color="textSecondary">
              PRODUCT PERSONALITY:
            </Typography>
            <Typography variant="body2" color="textSecondary">
              Coastguard is made of hot dipped galvanized coil that enhances the
              life of these tubes to provide better durability and strength. The
              interior coating of zinc helps in protecting the rusting of tubes
              from the inside. Built to last for generations, these tubes also
              provide excellent corrosion resistance against wind, water and
              road salts making them 100% rust proof.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default index;
